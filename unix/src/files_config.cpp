#include "files_config.h"

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>

#include <iostream>
#include <algorithm>
#include <sstream>
#include <vector>
#include <string>

#define TWICE_PAR cout << "\n\n"

using std::cout;
using std::cin;
using std::endl;
using std::cerr;
using std::ifstream;

namespace dirandfile {

  // FilesConfiguration class implemention
  std::string FilesConfiguration::CurPath()
  {
      const int size = 512;
      char buffer[size];
      std::string path;

      FILE * shell = popen("pwd", "r"); // reading from bash by ls
      if(!shell){
          perror("Popen failed!");
          exit(EXIT_FAILURE);
      }
      while (!feof(shell)) {
          if(fgets(buffer, size, shell) != NULL)
               path += buffer;
      }
      return path;
  }

  std::string FilesConfiguration::ParseChar(const char * path)
  {
      std::string folder;
      char *copy = strdup(path);
      if (copy == NULL){
          cerr << "NULL pointer is catched!" << endl;
          exit(EXIT_FAILURE);
      }

      char *tok = copy;
      char *end = copy;
      while (tok != NULL) {
          strsep(&end, "/");
          folder = tok;
          tok = end;
      }
      free(copy);
      return folder;
  }

  std::string FilesConfiguration::ParseString(const std::string &path)
  {
      std::stringstream stream(path);
      std::string segment;
      std::string back;
      while( std::getline(stream, segment, '/')){
          back = segment;
      }
      back.erase(std::find(back.begin(), back.end(), '\n'));
      return back;
  }

  void FilesConfiguration::DirectoryUp(const std::string &next)
  {
      std::string path = CurPath();
      path = path + '/' + next;
      // Element of parsing path that delete '\n' form concatenated string
      path.erase(std::find(path.begin(), path.end(), '\n'));
      chdir(path.c_str()); // go to remote directory
  }

  std::vector<std::string> & FilesConfiguration::GetCapacity(const std::string &dir, std::vector<std::string> &files)
  {
      chdir("..");
      DIR * disk;
      struct dirent *directory;
      disk = opendir(dir.c_str());
      if (disk == NULL){
          cerr << "Error! Can't open directory in find():" << endl;
          system("pwd");
          exit(EXIT_FAILURE);
      }
      while ((directory = readdir(disk))){
          files.push_back(directory->d_name); // string
      }
      closedir(disk);
      return files;
  }

  void FilesConfiguration::ShowCapacity(const std::vector<std::string> &files)
  {
      cout << "List of files in directory: ";
      system("pwd");
      cout << endl;
      // Magic number used for ignoring
      // "pointers" files in directory
      for (unsigned int i = 0; i < files.size() - 2; i++){
          cout << files[i] << endl;
      }
  }

  FilesConfiguration::FilesConfiguration()
  {
      remote_dir = "Disk";
      Connect();
      if (Empty()){
          cout << "Please add file to remote or enter 'r' to refresh and 'q' to exit!" << endl;
          char choise;
          do {
              cin.get(choise);
              cin.get();
              switch (choise) {
              case 'r':
              case 'R':
                  if (Empty()){
                      cout << "Empty directory!" << endl;
                  } else {
                      cout << "Not empty directory!" << endl;
                  }
                  break;
              case 'q':
              case 'Q':
                  cout << "Exit!" << endl;
                  if (Empty()){
                    cout << "Remote directory is empty! No filse to send!" << endl;
                    exit(EXIT_FAILURE);
                  }
                  break;
              default:
                  cout << "Bad input character! " << endl;
                  break;
              }
          } while (!((choise == 'q') || (choise == 'Q')));

      }
      cout << "List of files in directory Disk/:" << endl;
      system("ls");
      TWICE_PAR;
  }

  void FilesConfiguration::Connect()
  {
      chdir("..");
      cout << "Finding file in directiry:" << endl;
      system("pwd");

      if (Find()){
          cout << "File for remote sending has found successfully!" << endl;
      } else{
          if (Create()){
              cout << "Directory has created successfully in path: " << endl;
              system("pwd");
          }
      }
      DirectoryUp(remote_dir);
  }

  bool FilesConfiguration::Find()
  {
      std::string folder = ParseString(CurPath());
      std::vector<std::string> file;
      file = GetCapacity(folder, file);

      DirectoryUp(folder);

      if (std::find(file.begin(), file.end(), remote_dir) != file.end()){
          return 1;
      } else {
          return 0;
      }
  }

  bool FilesConfiguration::Create()
  {
      cout << "Creating directory Disk/ in:\n" << endl;
      system("pwd");
      system("mkdir Disk"); // create directory

      if (Find()){
          return true;
      } else {
          cerr << "Can't create directory!" << endl;
          exit(EXIT_FAILURE);
      }
  }

  bool FilesConfiguration::Empty()
  {
      std::vector<std::string> file;
      file = GetCapacity(remote_dir, file);
      size_t count = file.size();

      DirectoryUp(remote_dir);

      if (count <= 2) {
          return true;
      } else
          return false;
  }

  std::vector<std::string> FilesConfiguration::ListToSend()
  {
      std::string path = ParseString(CurPath());
      std::vector<std::string> files;
      files = GetCapacity(path, files);
      DirectoryUp(remote_dir);
      // Magic number '2' used for leave out two
      // "pointers" files in directory
      for (unsigned int i = 0; i < 2; i++){
          files.pop_back();
      }
      return files;
  }

}
