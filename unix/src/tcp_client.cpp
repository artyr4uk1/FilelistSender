#include "tcp_client.h"
#include "socket_utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <string>
#include <iostream>
#include <fstream>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

namespace clientdata {

  // Definitions of ClientConnection
  using utilites::socketutils::FillAddress;
  using utilites::socketutils::RecvInt;
  using utilites::socketutils::SendInt;
  using utilites::socketutils::Recieve;

  ClientConnection::ClientConnection() {
      Initialize(communicate, COMPORT);  // commucation socket
      cout << "Client is ready to communication..." << endl;
      Initialize(exchange, TRANSFER);  // transfer socket
      cout << "Client is ready to send data via specific port..." << endl;
  }

  void ClientConnection::Initialize(int & sock,const int &port)
  {
      sock = socket(AF_INET, SOCK_STREAM, 0);
      if(sock < 0){
          perror("Can't create socket connection on client side!");
          exit(EXIT_FAILURE);
      }
      address = FillAddress(address, port, 0);

      if (connect(sock, (struct sockaddr *)&address, sizeof(address)) < 0) {
          perror("Error: connection problems!");
          exit(EXIT_FAILURE);
      }
  }

  void ClientConnection::SendFile(int & sock, const char * file_name, size_t size_name, char *answer)
  {
      // Realized by 5 steps:
      // sending a size of name of file to send;
      // sending a name of file;
      // sending a size of file;
      // sending a file.
      // Recieve an answer about progress.
      std::ifstream s_file;
      s_file.open(file_name, std::ios::in|std::ios::binary|std::ios::ate);
      if(!s_file.is_open()){
          cerr << "Cannot open file " << file_name << endl;
          exit(EXIT_FAILURE);
      }
      cout << "File name: " << file_name << " File name size: " << size_name << endl;
      SendInt(sock, size_name);
      send(sock, file_name, size_name, 0);

      int size_file = s_file.tellg();
      cout << "Size of file to send: " << size_file << endl;
      SendInt(sock, size_file);

      char store[size_file];
      s_file.read(store, size_file);
      send(sock, store, size_file, 0);
      s_file.close();
      RecieveAnswer(sock, answer);
  }

  void ClientConnection::RecieveAnswer(int & sock, char *buffer)
  {
      int size_name;
      RecvInt(sock, size_name);
      Recieve(sock, 1, size_name, buffer);
      cout << "File \"" << (char*)buffer << "\" was sent successfully!"  << endl;
  }

  void ClientConnection::SendList(const std::vector<std::string> &files)
  {
      const unsigned int size = 256;  // max recieved size
      char buf[size];

      cout << "Sending list of files to server..." << endl;
      cout << files.size() << " files to send." << endl;
      SendInt(communicate, files.size());  // send to server count of files to recieve
      TWICE_PAR;

      for (unsigned int i = 0; i < files.size(); i++){
          std::string file = files[i];
          size_t lenght = file.size() + 1;
          cout << "Sending file \"" << file <<"\" with size " << lenght << endl;
          SendFile(exchange, file.c_str(), lenght, buf);
          TWICE_PAR;
      }
      cout << "Done!" << endl;
  }

  void ClientConnection::Disconnect()
  {
      close(exchange);
      close(communicate);
  }

}
