#include "include/tcp_client.h"
#include "include/files_config.h"

#include <string>
#include <vector>

using clientdata::ClientConnection;
using dirandfile::FilesConfiguration;

// Contain list of files to send
std::vector<std::string> files;
// The number of files to send
size_t count;

int main()
{
  FilesConfiguration config;
  files = config.ListToSend();
  count = files.size();
  ClientConnection con;
  con.SendList(files);
  con.Disconnect();
  return 0;
}
