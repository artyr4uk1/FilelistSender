#ifndef CLIENT_H
#define CLIENT_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <vector>
#include <string>

#define TWICE_PAR cout << "\n\n"
#define COMPORT 7500
#define TRANSFER 7505

namespace clientdata {

  // Declarations of client data
  class ClientConnection
  {
  private:
      int communicate;
      int exchange;
      struct sockaddr_in address;
      // Initializes socket connection
      void Initialize(int & sock,const int &port);
  public:
      ClientConnection();
      void SendFile(int &sock, const char *file_name, size_t size_name, char *answer);
      void RecieveAnswer(int &sock, char *buffer);
      void SendList(const std::vector<std::string> & files);
      void Disconnect();
  };
}

#endif // CLIENT_H
