#ifndef SOCKET_UTILS_H
#define SOCKET_UTILS_H

#include <netinet/in.h>

namespace utilites {

  // Declarations of utilites for client/server connection
  namespace socketutils {

    // Declarations of socket utilites
    // Copies information to memory block
    // and returning an address to last copied cell
    char * Cpy(char * dest, char * src, size_t size);
    // Fills in internet address
    sockaddr_in & FillAddress(struct sockaddr_in & address, const int &port, bool side);
    // Sends an int value
    void SendInt(int &sock, const int &to_send);
    // Receives an int value
    void RecvInt(int &sock, int &to_recv);
    // Recieves data
    void Recieve(int &sock, int def_block, long size, char* buffer);
  }
}

#endif  // SOCKET_UTILS_H
