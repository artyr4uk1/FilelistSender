#ifndef FILESCONFIG_H
#define FILESCONFIG_H

#include <vector>
#include <string>

namespace dirandfile{

  // Declarations of data that provides
  // working with files and directories.
  class FilesConfiguration{
  private:
      const char * remote_dir;
      // Returns current path to directiry
      std::string CurPath();
      // Both parse functions returns last word after '/' in path
      std::string ParseChar(const char *);
      std::string ParseString(const std::string & path);
      // Move in path to next "passed" directiry
      void DirectoryUp(const std::string & next);
      // Returns a contents of directory
      std::vector<std::string> & GetCapacity(const std::string & dir, std::vector<std::string> & files);
      // Reflects directory capacity
      void ShowCapacity(const std::vector<std::string> & files);
  public:
      FilesConfiguration();
      // Looking for remote directory
      // or creating it
      void Connect();
      bool Find();
      bool Create();
      bool Empty();
      // Returns a list of files to send
      std::vector<std::string> ListToSend();
  };
}
#endif // FILESCONFIG_H
