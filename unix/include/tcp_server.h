#ifndef SERVER_H
#define SERVER_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#define TWICE_PAR cout << "\n\n"
#define BACK_LOG 50
#define COMPORT 7500
#define TRANSFER 7505

namespace serverdata {

  // Declarations of ServerConnection
  class ServerConnection
  {
  private:
      int server_sock;
      int new_sock;
      int exchange;
      int new_exchange;
      int client_len;
      struct sockaddr_in address;
      struct sockaddr_in client;
      // Initializes socket connection
      void Initialize(int & sock,  const int & port);
  public:
      ServerConnection();

      void RecieveData(int &sock, char *buffer, int max_size);
      void RecieveList();
      // Reflects status of connection
      void Status();
      void Disconnect();
  };
}
#endif // SERVER_H
