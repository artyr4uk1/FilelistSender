ifeq ($(OS),Windows_NT)
	DIR = windows
else
	DIR = 'unix'
endif

all:
	cd $(DIR) && $(MAKE)
