#include "include/tcp_client.h"
#include "include/files_config.h"

#include <stdio.h>
#include <string.h>

#include <string>
#include <vector>

using clientdata::ClientConnection;
using dirandfile::FilesConfiguration;

std::vector<std::string> files;
size_t count;

int main()
{
  FilesConfiguration config;
  files = config.ListToSend();
  count = files.size();
  ClientConnection con;
  con.SendList(files);
  con.Disconnect();
  return 0;
}
