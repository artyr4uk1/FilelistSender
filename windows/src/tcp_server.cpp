#include "tcp_server.h"
#include "socket_utils.h"

#include <stdio.h>
#include <stdlib.h>

#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

namespace serverdata {

  // Definitions of ServerConnection class

  using utilites::socketutils::FillAddress;
  using utilites::socketutils::RecvInt;
  using utilites::socketutils::SendInt;
  using utilites::socketutils::Recieve;
  using utilites::socketutils::InitializeWinsock;
  using utilites::socketutils::CloseWinsock;

  void ServerConnection::Initialize(int &sock, const int &port)
  {
      sock = socket(AF_INET, SOCK_STREAM, 0);
      if (sock == INVALID_SOCKET){
          printf("Can't create socket connection on server side! Code: %d", WSAGetLastError());
          exit(EXIT_FAILURE);
      }

      address = FillAddress(address, port, 1);

      if(bind(sock, (sockaddr*)&address, sizeof(address)) == SOCKET_ERROR){
          printf("Cannot bind by server socket! Code: %d", WSAGetLastError());
          exit(EXIT_FAILURE);
      }
      listen(sock, BACK_LOG);
  }

  ServerConnection::ServerConnection()
  {
      InitializeWinsock();
      Initialize(server_sock, COMPORT);
      Initialize(exchange, TRANSFER);
  }

  void ServerConnection::RecieveData(int & sock, char *buffer, int max_size)
  {
      // Realized by 5 steps:
      // receive a size of name of file to send;
      // receive a name of file;
      // receive a size of file;
      //receive a file.
      // Send an answer about progress.
      cout  << "Recieving file:" << endl;
      int size_name;
      RecvInt(sock, size_name);  // get a size of name of file
      cout  << "Size of name of file: " << size_name << endl;

      char * file_name = new char[size_name];
      Recieve(sock, 1, size_name, file_name);
      cout << "Name of file: " << file_name << endl;

      int size_file;
      RecvInt(sock, size_file);
	  
      if (size_file > max_size){
        cerr << "Size of file " << file_name << " is bigger than maximum recieved size!" << endl;
        exit(EXIT_FAILURE);
      }
      if (size_file <= 1024){
          Recieve(sock, 1, size_file, buffer);
      } else {
          Recieve(sock, 1024, size_file, buffer);
      }

      int net_size_file = htonl(size_name);
      cout << "File name to client for answer: " << file_name << endl;
      send(sock, (const char*)&net_size_file, 4, 0);  // send to client size of name of file
      send(sock, file_name, size_name, 0);  // send to client name of file

      delete[] file_name;
  }

  void ServerConnection::RecieveList()
  {
	  do {
		  const unsigned int size = 4096;
		  char buffer[size];

		  // Communication socket with client
		  client_len = sizeof(client);
		  new_sock = accept(server_sock, (struct sockaddr*)&client, &client_len);
		  if (new_sock < 0){
			  perror("Can't create new socket for connection with client!");
			  exit(EXIT_FAILURE);
		  }

		  // Exchanging socket with client
		  client_len = sizeof(client);
		  new_exchange = accept(exchange, (struct sockaddr*)&client, &client_len);
		  if (new_exchange < 0){
			  perror("Can't create new socket for connection with client!");
			  exit(EXIT_FAILURE);
		  }

		  int count;  // count of files to receive
		  RecvInt(new_sock, count);
		  cout << "Server has required to recieve " << count << " files..." << endl;

		  TWICE_PAR;
		  for (int i = 0; i < count; i++){
			  RecieveData(new_exchange, buffer, size);  // recieve file with max size 4096
			  TWICE_PAR;
		  }
		  cout << "Done!" << endl;
		  cout << "Receiving next list of files..." << endl;
	  } while(true);
  }

  void ServerConnection::Status()
  {
      cout << "Server started successfully!\n"
           << "And waiting for file from client" << endl;
      TWICE_PAR;
  }

  void ServerConnection::Disconnect()
  {
      CloseWinsock();
      closesocket(new_exchange);
      closesocket(new_sock);
  }

}
