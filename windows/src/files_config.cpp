#include "files_config.h"

#include <Windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <direct.h>
#include <string.h>

#include <iostream>
#include <algorithm>
#include <sstream>
#include <vector>
#include <string>

#define TWICE_PAR cout << "\n\n"

using std::cout;
using std::cin;
using std::endl;
using std::cerr;

namespace dirandfile {

  // Definitions of FilesConfiguration class
  std::string FilesConfiguration::CurPath()
  {
      const int buf_size = MAX_PATH;
      char old_dir[buf_size];  // store the current directory

      // Get the current directory, and store it
      if (!GetCurrentDirectory(buf_size, old_dir)) {
          std::cerr << "Error getting current directory: #" << GetLastError();
      }
      return old_dir;
  }

  std::string FilesConfiguration::ParsePath(std::string path)
  {
      std::string change = path;
      for(std::string::iterator it = std::find(path.begin(), path.end(), '\\'); it != path.end(); it = std::find(it, path.end(), '\\')){
          it = path.insert(it, '\\') + 2;
      }
      // Get the path that ends: "\\example\\*"
      path = path + "\\" + "\\" + "*";
      return path;
  }

  std::string FilesConfiguration::ParseString(const std::string &path)
  {
      std::stringstream stream(path);
      std::string segment;
      std::string back;
      while( std::getline(stream, segment, '\\')){
          back = segment;
          cout << back << endl;
      }
      return back;
  }

  std::vector<std::string> & FilesConfiguration::GetCapacity(const std::string &dir, std::vector<std::string> &files)
  {
    WIN32_FIND_DATA file;
    HANDLE search_handle = FindFirstFile(dir.c_str(),&file);
    if (search_handle)
    {
        do{
          files.push_back(file.cFileName);
        } while(FindNextFile(search_handle, &file));
        FindClose(search_handle);
    }
    return files;
  }

  void FilesConfiguration::ShowCapacity(const std::vector<std::string> &files)
  {
      std::cout << std::endl;
      // Magic number used for ignoring
      // "pointers" files in directory
      for (unsigned int i = 2; i < files.size(); i++){
          std::cout << files[i] << std::endl;
      }
  }

  FilesConfiguration::FilesConfiguration()
  {
      remote_dir = "Disk";
      Connect();
      if (Empty()){
          cout << "Please add file to remote or enter 'r' to refresh and 'q' to exit!" << endl;
          char choise;
          do {
              cin.get(choise);
              cin.get();
              switch (choise) {
              case 'r':
              case 'R':
                  if (Empty()){
                      cout << "Empty directory!" << endl;
                  } else {
                      cout << "Not empty directory!" << endl;
                  }
                  break;
              case 'q':
              case 'Q':
                  cout << "Exit!" << endl;
                  break;
              default:
                  cout << "Bad input character! " << endl;
                  break;
              }
          } while (!((choise == 'q') || (choise == 'Q')));

      }
      cout << "List of files in directory Disk/:" << endl;
      std::vector<std::string> files;
      GetCapacity(remote_path, files);
      ShowCapacity(files);
      TWICE_PAR;
  }

  void FilesConfiguration::Connect()
  {
      chdir("..");
      cout << "Finding file in directiry:" << endl;
      system("echo %cd%");

      remote_path = CurPath() + '\\' + remote_dir;
      remote_path = ParsePath(remote_path);

      if (Find()){
          cout << "File for remote sending has found successfully!" << endl;
      } else{
          if (Create()){
              cout << "Directory has created successfully in path: " << endl;
              system("echo %cd%");
          }
      }
      DirectoryUp(remote_dir);
  }

  bool FilesConfiguration::Find()
  {
      std::string folder = CurPath();
      std::vector<std::string> file;
      file = GetCapacity(ParsePath(folder), file);
      if (std::find(file.begin(), file.end(), remote_dir) != file.end()){
          return 1;
      } else {
          return 0;
      }
  }

  bool FilesConfiguration::Create()
  {
      cout << "Creating directory Disk/ in:\n" << endl;
      system("echo %cd%");
      system("mkdir Disk");  // create directory

      if (Find()){
          return true;
      } else {
          cerr << "Can't create directory!" << endl;
          exit(EXIT_FAILURE);
      }
  }

  bool FilesConfiguration::Empty()
  {

      std::vector<std::string> file;
      file = GetCapacity(remote_path, file);
      size_t count = file.size();

      if (count <= 2) {
          return true;
      } else
          return false;
  }

  void FilesConfiguration::DirectoryUp(const std::string &next)
    {
        std::string path = CurPath();
        path = path + '\\' + next;
        chdir(path.c_str());  // go to remote directory
    }

  std::vector<std::string> FilesConfiguration::ListToSend()
  {
      std::string path = ParsePath(CurPath());
      std::vector<std::string> files;
      files = GetCapacity(path, files);
      // Magic number '2' used for leave out two
      // "pointers" files in directory
      for (unsigned int i = 0; i < 2; i++){
          files.erase(files.begin());
      }
      return files;
  }

}
