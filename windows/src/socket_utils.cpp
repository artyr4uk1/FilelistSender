#include "socket_utils.h"

#include <stdlib.h>
#include <winsock2.h>
#include <iostream>

namespace utilites {

  namespace socketutils {

    // Definitions of utilites
    void InitializeWinsock()
    {
        WSADATA wsa;
        if (WSAStartup(MAKEWORD(2,2),&wsa) != 0){
          std::cerr  << "Winsock Initialization failed! Code: " << WSAGetLastError() << std::endl;
          exit(EXIT_FAILURE);
        }
        std::cout << "Winsock initialized successfully!" << std::endl;
    }

    void CloseWinsock()
    {
        WSACleanup();
    }

    char *Cpy(char *dest, char *src, size_t size)
    {
        if ((dest == NULL) || (src == NULL)){
            std::cerr << "Argument is NULL pointer!" << std::endl;
            return NULL;
        }

        char* ldest = dest;
        const char* lsrc = (const char*)src;

        while(size){
            *(ldest++) = *(lsrc++);
            size--;
        }
        return ldest;
    }

    struct sockaddr_in &FillAddress(struct sockaddr_in & address, const int &port, bool side)
    {
        address.sin_family = AF_INET;
        address.sin_port = htons(port);
        if (side == 1){
            address.sin_addr.s_addr = htonl(INADDR_ANY);
        } else {
            address.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
        }
        return address;
    }

    void SendInt(int &sock, const int &to_send)
    {
        int net_int = htonl(to_send);
        send(sock, (const char*)&net_int, 4, 0);
    }

    void RecvInt(int &sock, int &to_recv)
    {
        int net_int;
        if (recv(sock, (char*)&net_int, 4, 0) <= 0){
            std::cerr << "Error: can't recieve an INT value!" << std::endl;
            exit(EXIT_FAILURE);
        }
        to_recv = ntohl(net_int);
    }

    void Recieve(int &sock, int def_block, long size, char *buffer)
    {
        long total_recieved = 0;
        char* tmp_buff = new char[def_block];
        while (total_recieved < size) {
            int recieved = recv(sock, tmp_buff, def_block, 0);
            buffer = Cpy(buffer, tmp_buff, recieved);
            total_recieved += recieved;
        }
        delete[] tmp_buff;
    }
    
  }
}
