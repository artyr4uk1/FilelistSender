#ifndef FILESCONFIG_H
#define FILESCONFIG_H

#include <vector>
#include <string>

namespace dirandfile{

  // Declarations of data that provide:
  // working with files and directories
  class FilesConfiguration{
  private:
      const char * remote_dir;
      std::string remote_path;
      // Returns current path to directiry
      std::string CurPath();
      // Returns path with '\\' slashes instead '\' as default
      std::string ParsePath(std::string path);
      // Returns last word after '\' in path
      std::string ParseString(const std::string &path);
      // Move in path to next "passed" directiry
      void DirectoryUp(const std::string & next);
      // Returns a contents of directory
      std::vector<std::string> & GetCapacity(const std::string & dir, std::vector<std::string> & files);
      // Reflects directory capacity
      void ShowCapacity(const std::vector<std::string> & files);
  public:
      FilesConfiguration();
      void SetupDirectory();
      // Looking for remote directory
      // or creating it
      void Connect();
      bool Find();
      bool Create();
      bool Empty();
      // Returns a list of files to send
      std::vector<std::string> ListToSend();
  };
}
#endif // FILESCONFIG_H
