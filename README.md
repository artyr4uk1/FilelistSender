Client/Server program to load files from remote HOST.
- Communication in between client and server occur by port 7500;
- client sends list of files in his folder;
- server requested to load one file from the list via port 7505;
- client sends files to the server via port 7505.

Use Makefile to compile a project on Unix or Windows.
